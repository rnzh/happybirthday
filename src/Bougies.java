
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LTC_134_016
 */
public class Bougies {
    private ArrayList<Bougie>alBougies=new ArrayList<>();
    public void draw(Graphics g,int w,int h){
        if(alBougies.isEmpty()){
            g.setColor(Color.BLACK);
            g.fillRect(0,0,w,h);
            
        }
        for(int i=0;i<alBougies.size();i++){
            alBougies.get(i).draw(g,w,h);
        }       
    }

    public boolean isEmpty() {
        return alBougies.isEmpty();
    }

    public int size() {
        return alBougies.size();
    }

    public Object[] toArray() {
        return alBougies.toArray();
    }

    public boolean add(Bougie e) {
        return alBougies.add(e);
    }

    public void clear() {
        alBougies.clear();
    }
    public void doStep(){
        for(int i=0;i<alBougies.size();i++){
            alBougies.get(i).doStep();
            if(alBougies.get(i).getAge()==50){
                alBougies.remove(i);
            }
        }
            
    }

    public void blink(){
        for(int i=0;i<alBougies.size();i++){
            alBougies.get(i).blink();
        }
    }
    public void trie(){
        for(int i=0;i<alBougies.size()-1;i++){
            int min=i;
            for(int j=i+1;j<alBougies.size();j++){
                if(alBougies.get(min).getY()>alBougies.get(j).getY()){
                    min=j;
                }
            }
            if(min!=i){
                Bougie bougie=alBougies.get(i);
                alBougies.set(i, alBougies.get(min));
                alBougies.set(min, bougie);
            }
        }
    }
}
