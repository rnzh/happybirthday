
import java.awt.Color;
import java.awt.Graphics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LTC_134_016
 */
public class DrawPanel extends javax.swing.JPanel {

    private Bougies bougies;
    private FireWorks fires;
    public DrawPanel() {
        initComponents();
    }

    public void paintComponent(Graphics g){
        int r=getWidth()/7*5/8;
        g.setColor(new Color(255,154,7));
        g.fillRect(0,0,getWidth(),getHeight());
        g.setColor(new Color(250,130,160));
        g.fillRect(getWidth()/7,getHeight()/2,getWidth()/7*5,getHeight()/2);
        g.setColor(new Color(240,50,66));
        for(int i=0;i<4;i++){
            g.fillOval(getWidth()/7+(i*2*r),getHeight()/2-10 ,2*r, 2*r);
        }
        g.setColor(new Color(255,182,193));
        g.fillOval(getWidth()/7,getHeight()/2-50,getWidth()/7*5,100);
        if(bougies!=null)bougies.draw(g,getWidth(), getHeight());
        if(fires!=null)fires.draw(g);
    }

    public void setFires(FireWorks fires) {
        this.fires = fires;
    }

    public void setBougies(Bougies bougies) {
        this.bougies = bougies;
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
