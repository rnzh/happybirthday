
import java.awt.Graphics;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author zheng
 */
public class FireWorks {
    private ArrayList<Fire>alFires=new ArrayList<>();
    public void draw(Graphics g){
        for(int i=0;i<alFires.size();i++){
            alFires.get(i).draw(g);
        }
    }
    
    public boolean add(Fire e) {
        return alFires.add(e);
    }

    public void clear() {
        alFires.clear();
    }
    public void step(){
        for(int i=0;i<alFires.size();i++){
            alFires.get(i).step();
            if(alFires.get(i).getSize()>30){
                alFires.remove(i);
            }
        }
    }
}
