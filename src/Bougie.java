
import java.awt.Color;
import java.awt.Graphics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LTC_134_016
 */
public class Bougie {
    private int x;
    private int y;
    private Color col;
    private int age=0;
    private boolean blinking=false;
    public Bougie(int w,int h) {
        x=(int)(Math.random()*((w/7*6-40)-(w/7+40))+1)+w/7+40;
        y=(int)(Math.random()*((h/2+25)-(h/2-20)+1))+h/2-20;
        col=new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256));
    }
    public void doStep(){
        age++;
    }

    public int getY() {
        return y;
    }

   
    
    public int getAge() {
        return age;
    }
    public void draw(Graphics g,int w,int h){
        int l=50;
        l=50-age;
        g.setColor(col);
        g.fillRect(x-5,y-l,10,l);
        g.setColor(Color.BLACK);
        g.drawRect(x-5,y-l,10,l);
        if(blinking==false){
            g.setColor(Color.yellow);
            for(int i=0;i<16;i++){
            g.drawLine(x-8+i,y-l-5,x, y-l-20);            
        }
        g.fillOval(x-8,y-l-11,16,11);
        }
        else{
            g.setColor(new Color(240,36,12));
            for(int i=0;i<16;i++){
            g.drawLine(x-8+i,y-l-5,x, y-l-20);            
        }
            g.fillOval(x-8,y-l-11,16,11);
        }
            
         
        
    }
    public void blink(){
        if(blinking){
            blinking=false;
        }
        else blinking=true;
    }

}
