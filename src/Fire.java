
import java.awt.Color;
import java.awt.Graphics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author zheng
 */
public class Fire {
    private int size=0;
    private int dx;
    private int dy;
    private Color col;
    public Fire(int w,int h){
        dx=(int)(Math.random()*(w));
        dy=(int)(Math.random()*(h)); 
        col=new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256));
    }
    public void step(){
        size++;
    }

    public int getSize() {
        return size;
    }
    public void draw(Graphics g){
                int r=size;
                g.setColor(col);
                if(size<30){
                    g.fillOval(dx-r, dy-r, 2*r, 2*r);
                }    
                if(size==30){          
                    g.setColor(Color.yellow);
                    g.drawLine(dx-30, dy-30,dx+30 , dy+30);
                    g.drawLine(dx-30, dy+30,dx+30 , dy-30);
                    g.drawLine(dx, dy-30,dx , dy+30);
                    g.drawLine(dx-30, dy,dx+30 , dy);
            }
    }
}
